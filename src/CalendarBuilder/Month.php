<?php

namespace Domatskiy\Calendar\CalendarBuilder;

use Domatskiy\Calendar\CalendarBuilder\Month\Day;

use Domatskiy\Calendar\NotCorrectMonthException;

class Month
{
    /**
     * @var $month int
     * @var $events Event[]
     */
    private
        $year,
        $month,
        $days = [];

    private $calendar;

    /**
     * Month constructor.
     * @param int $year
     * @param int $month
     * @param $events Event[]
     * @throws NotCorrectMonthException
     */
    function __construct(int $year, int $month, array $events = [])
    {
        if($month < 1 || $month > 12)
            throw new NotCorrectMonthException('not correct month');

        $this->year = $year;
        $this->month = $month;

        if(!empty($events) && $events)
        {
            foreach ($events as $event)
            {
                /**
                 * @var $event Event[]
                 */
                if(!($event instanceof Event))
                    throw new \Exception('not correct event for month '.$this->month);

                if($event->getMonth() !== $this->month)
                    continue;

                if(!array_key_exists($event->getDay(), $this->events))
                    $this->events[$event->getDay()] = [];

                $this->events[$event->getDay()][] = $event;
            }
        }
    }

    /**
     * @param Day $day
     * @throws \Exception
     */
    function addDay(Day $day)
    {
        $this->calendar = null;

        if($day->getMonth() !== $this->month)
            throw new \Exception('event month '.$day->getMonth().' / current month '.$this->month);

        if(!array_key_exists($day->getDay(), $this->days))
            $this->days[$day->getDay()] = null;
        elseif($this->days[$day->getDay()])
            throw new \Exception('day exits: '.$day->getDay().'.'.$this->month.'.'.$this->year);

        $this->days[$day->getDay()] = $day;
    }

    function calcHourWork($hour): int
    {
        $calendar = $this->getCalendar();

        $count_hour = 0;

        foreach ($calendar as $day)
        {
            if(!$day->isWork())
                continue;

            /**
             * @var $day Day
             */
            $count_hour += $day->getWorkHour();
        }

        return $count_hour;
    }

    function getCalendar(): array
    {
        if(is_array($this->calendar))
            return $this->calendar;

        $this->calendar = [];

        $first_day = mktime(0,0,0, $this->month, 1, $this->year);
        $last_day = mktime(0,0,0, $this->month + 1, 0, $this->year);

        # $LastMonday = new \DateTime();
        # $LastMonday->setTimestamp($first_day);
        # $LastMonday->modify('last Monday');
        # echo $LastMonday->format('Y-m-d');

        $Date1 = new \DateTime();
        $Date1->setTimestamp($first_day);

        $Date2 = new \DateTime();
        $Date2->setTimestamp($last_day);

        #dd($Date1->format('Y-m-d'), $Date2->format('Y-m-d'));
        #dd($Date1->format('d'), $Date2->format('d'));

        for ($day=(int)$Date1->format('d'); $day <= (int)$Date2->format('d'); $day++)
        {
            $day = (int)$day;

            $__day = array_key_exists($day, $this->days) ? $this->days[$day] : null;

            if($__day)
            {
                $this->calendar[$day] = $__day;
            }
            else
            {
                $CDay = new Day($this->year, $this->month, $day);
                $CDay->setWork();
                $CDay->setWorkTime(8);
                $this->calendar[$day] = $CDay;
            }
        }

        #dd($data);

        return $this->calendar;
    }
}
