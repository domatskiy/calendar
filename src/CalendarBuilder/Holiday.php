<?php

namespace Domatskiy\Calendar\CalendarBuilder;

class Holiday
{
    private
        $id,
        $day,
        $month,
        $name;

    function __construct(int $month, int $day, string $name, int $id = null)
    {
        $this->month = $month;
        $this->day = $day;
        $this->name = trim($name);
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getDay(): int
    {
        return $this->day;
    }

    /**
     * @return int
     */
    public function getMonth(): int
    {
        return $this->month;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->id;
    }
}