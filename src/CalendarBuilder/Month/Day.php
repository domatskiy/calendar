<?php

namespace Domatskiy\Calendar\CalendarBuilder\Month;

use Domatskiy\Calendar\CalendarBuilder\Event;
use Domatskiy\Calendar\CalendarBuilder\Holiday;

class Day
{
    private
        $year,
        $month,
        $day,
        $time,

        $is_pre_holiday = false, // pre-holiday day
        $is_weekend = false,

        $work_name = '',
        $work_hour = null,
        $is_work = false,

        $day_number,

        $events = [],
        $holiday = [];

    /**
     * Day constructor.
     * @param int $year
     * @param int $month
     * @param int $day
     */
    function __construct(int $year, int $month, int $day)
    {
        $this->year = $year;
        $this->month = $month;
        $this->day = $day;

        $this->time = strtotime($day.'.'.$month.'.'.$year);
        $this->day_number = (int)date('N', $this->time);
        $this->is_weekend = $this->day_number === 0 || $this->day_number === 6;
    }

    /**
     * @param Holiday $holiday
     * @return Day
     */
    function addHoliday(Holiday $holiday): Day
    {
        $this->work_name = '';
        $this->is_work = false;
        $this->work_hour = 0;

        $this->holiday[] = $holiday;

        return $this;
    }

    /**
     * @param Event $event
     * @return Day
     */
    function addEvent(Event $event):Day
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * @param bool $is_pre_holiday
     * @return Day
     * @throws \Exception
     */
    public function setPreHoliday(bool $is_pre_holiday = true):Day
    {
        if(!empty($this->holiday))
            throw new \Exception($this->getDate().' can not be work day, it has '.count($this->holiday).' holidays');

        $this->is_pre_holiday = $is_pre_holiday;

        return $this;
    }

    /**
     * @param string $name
     * @param bool $is_work
     * @return Day
     * @throws \Exception
     */
    public function setWork(string $name = '', bool $is_work = true):Day
    {
        if(!empty($this->holiday))
            throw new \Exception($this->getDate().' can not be work day, it has '.count($this->holiday).' holidays');

        $this->work_name = $name;
        $this->is_work = $is_work;

        return $this;
    }

    /**
     *
     * @param int $hour
     * @return Day
     * @throws \Exception
     */
    public function setWorkTime(float $hour):Day
    {
        if(!empty($this->holiday))
            throw new \Exception($this->getDate().' can not be work day, it has '.count($this->holiday).' holidays');

        $this->work_hour = $hour;

        return $this;
    }


    /**
     * @return string
     */
    function getDate(): string
    {
        return $this->day.'.'.$this->month.'.'.$this->year;
    }

    /**
     * @return int
     */
    function getDay(): int
    {
        return $this->day;
    }

    function getMonth(): int
    {
        return $this->month;
    }

    function getYear(): int
    {
        return $this->year;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return (int)$this->day_number;
    }

    /**
     * @return Holiday[]
     */
    function getHolidays()
    {
        return $this->holiday;
    }

    /**
     * @return Event[]
     */
    function getEvents(): array
    {
        return $this->events;
    }

    /**
     * @return bool
     */
    function isWeekend():bool
    {
        return $this->is_weekend;
    }

    /**
     * @return bool
     */
    function isPreHoliday():bool
    {
        return $this->is_pre_holiday;
    }

    /**
     * @return bool
     */
    function isWork():bool
    {
        return $this->is_work;
    }

    /**
     * @return float
     */
    public function getWorkHour(): float
    {
        return $this->work_hour;
    }
}
