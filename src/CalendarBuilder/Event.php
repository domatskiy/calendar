<?php

namespace Domatskiy\Calendar\CalendarBuilder;

class Event
{
    private
        $id,
        $name;

    function __construct(string $name, int $id = null)
    {
        $this->name = trim($name);
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->id;
    }
}