<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'day1' => 'ПН',
    'day2' => 'ВТ',
    'day3' => 'СР',
    'day4' => 'ЧТ',
    'day5' => 'ПТ',
    'day6' => 'СБ',
    'day7' => 'ВС',

    'month1' => 'Январь',
    'month2' => 'Февраль',
    'month3' => 'Март',
    'month4' => 'Апрель',
    'month5' => 'Май',
    'month6' => 'Июнь',
    'month7' => 'Июль',
    'month8' => 'Август',
    'month9' => 'Сентябрь',
    'month10' => 'Октябрь',
    'month11' => 'Ноябрь',
    'month12' => 'Декабрь',

    'month_r1' => 'Января',
    'month_r2' => 'Февраля',
    'month_r3' => 'Марта',
    'month_r4' => 'Апреля',
    'month_r5' => 'Мая',
    'month_r6' => 'Июня',
    'month_r7' => 'Июля',
    'month_r8' => 'Августа',
    'month_r9' => 'Сентября',
    'month_r10' => 'Октября',
    'month_r11' => 'Ноября',
    'month_r12' => 'Декабря',
    

];
