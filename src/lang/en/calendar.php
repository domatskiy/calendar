<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'day1' => 'ПН',
    'day2' => 'ВТ',
    'day3' => 'СР',
    'day4' => 'ЧТ',
    'day5' => 'ПТ',
    'day6' => 'СБ',
    'day7' => 'ВС',

    'month1' => 'Январь',
    'month2' => 'Февраль',
    'month3' => 'Март',
    'month4' => 'Апрель',
    'month5' => 'Март',
    'month6' => 'Май',
    'month7' => 'Июнь',
    'month8' => 'Август',
    'month9' => 'Сентябрь',
    'month10' => 'Октсябрт',
    'month11' => 'Ноябрь',
    'month12' => 'Декабрь',

];
