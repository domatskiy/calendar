<?php

namespace Domatskiy\Calendar;

use Domatskiy\Calendar\CalendarBuilder\Month;
use Domatskiy\Calendar\CalendarBuilder\Month\Day;

class CalendarBuilder
{
    /**
     * @var $days []
     */
    protected
        $year,
        $data = null,
        $days = [],
        $holidays = [];

    protected
        $workDayNumber = 5;

    function __construct(int $year)
    {
        $this->year = $year;
    }

    /**
     * @return int
     */
    public function getWorkDayNumber()
    {
        return (int)$this->workDayNumber;
    }

    /**
     * @param int $days
     * @return CalendarBuilder
     */
    public function setWorkDayNumber(int $days): CalendarBuilder
    {
        $this->workDayNumber = $days;
        return $this;
    }

    /**
     * @param Day $day
     */
    public function addDay(Day $day)
    {
        // reset computed data
        $this->data = null;

        // init month
        if(!array_key_exists($day->getMonth(), $this->days))
            $this->days[$day->getMonth()] = [];

        // init day of the month
        if(!array_key_exists($day->getDay(), $this->days[$day->getMonth()]))
            $this->days[$day->getMonth()][$day->getDay()] = [];

        // add day to the month
        $this->days[$day->getMonth()][$day->getDay()][] = $day;
    }

    /**
     * @throws \Exception
     */
    public function getCalendar(int $from_month = 1, int $to_month = 12): array
    {
        if(!$from_month || $from_month < 1 || $from_month > 12)
            throw new NotCorrectMonthException('not correct start month');

        if(!$to_month || $to_month < 1 || $to_month > 12)
            throw new NotCorrectMonthException('not correct end month');

        if($from_month > $to_month)
            throw new NotCorrectDateDiapasonException('not correct month diapason');

        # TODO save computed data

        #if(is_array($this->data))
        #    return $this->data;

        $this->data = [];

        for ($month=$from_month;$month<=$to_month;$month++)
        {
            // create month
            $CMonth = new Month($this->year, $month);

            $days = array_key_exists($month, $this->days) ? $this->days[$month] : [];

            if(!empty($days))
            {
                // add custom days to the month
                foreach ($days as $day_data)
                {
                    foreach ($day_data as $day)
                        $CMonth->addDay($day);
                }
            }

            $this->data[$month] = $CMonth;
        }

        return $this->data;
    }
}
