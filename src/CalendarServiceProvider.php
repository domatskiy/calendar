<?php

namespace Domatskiy\Calendar;

use Illuminate\Support\ServiceProvider;

class CalendarServiceProvider extends ServiceProvider
{
    /**
     * Bootstrapping the package.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/calendar.php' => config_path('calendar.php'),
        ], 'calendar');

    }

}
