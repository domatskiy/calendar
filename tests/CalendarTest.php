<?php

namespace Domatskiy\Calendar\Tests;

use Domatskiy\Calendar\CalendarBuilder;
use Domatskiy\Calendar\CalendarBuilder\Month;
use Domatskiy\Calendar\CalendarBuilder\Month\Day;
use Domatskiy\Calendar\Facades\Calendar;
use Domatskiy\Calendar\NotCorrectMonthException;
use PHPUnit\Framework\TestCase as BaseTestCase;

class CalendarTest extends BaseTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testDay()
    {
        $day = new Day(2018, 12, 1);

        $this->assertInstanceOf(Day::class, $day);
        $this->assertTrue($day->getYear() === 2018);
        $this->assertTrue($day->getMonth() === 12);
        $this->assertTrue($day->getDay() === 1);

        try {

            new Day(2018, 13, 1);
            $not_correct_month = true;

        } catch (\Exception $e) {

            $not_correct_month = false;
        }

        $this->assertTrue($not_correct_month, 'test not correct month');

    }

    public function testMonth()
    {
        $year = (int)date('m');

        try {

            new Month($year, 13);
            $this->assertTrue(false, 'created month for not correct date');

        } catch (\Exception $e) {

            $this->assertInstanceOf(NotCorrectMonthException::class, $e, 'Not correct exception of the month');

        }

        for($month=1;$month<=12;$month++)
        {
            $CMonth = new Month($year, $month);
        }

    }

    public function testCalendar()
    {
        $year = (int)date('m');

        $CCalendar = new CalendarBuilder($year);

        /**
         * test error periods
         */

        $CCalendar->getCalendar(1);
    }

}
