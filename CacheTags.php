<?php

namespace Domatskiy\TaggedCache;

use Illuminate\Support\Facades\Schema;
use Domatskiy\TaggedCache\DB\CacheKey;
use Domatskiy\TaggedCache\DB\CacheKeyTag;

class CacheTags extends \Illuminate\Support\Facades\Cache
{

    /**
     * @param string $key
     * @param \DateInterval|\DateTimeInterface|float|int $minutes
     * @param array $tags
     * @param \Closure $callback
     * @return mixed
     * @throws \Exception
     */
    public static function remember($key, $minutes, array $tags, \Closure $callback)
    {
        if(!parent::has($key))
        {
            $cache = CacheKey::firstOrCreate([
                'key' => $key
                ]);

            if(empty($tags))
            {
                # Log::debug('CACHE: clear tags');

                CacheKeyTag::where('cache_id' , '=', $cache->id)
                    ->delete();
            }
            else
            {
                foreach ($tags as $tag)
                {
                    CacheKeyTag::firstOrCreate([
                        'cache_id' => $cache->id,
                        'tag' => $tag
                    ]);
                }
            }

        }

        $rem_result = parent::remember($key, $minutes, $callback);

        return $rem_result;
    }

    public static function deleteByTag($tag): int
    {
        \DB::beginTransaction();

        $tags = CacheKeyTag::select([
            'cache_tags.cache_id',
            'cache.key as cache_key',
            ])
            ->where('cache_tags.tag', '=', $tag)
            ->join('cache', 'cache_tags.cache_id', '=', 'cache.id')
            ->groupBy(['cache_tags.cache_id'])
            ->get();

        $count = 0;

        if($tags->count() === 0)
        {
            return $count;
        }

        foreach ($tags as $tag)
        {
            $res = parent::delete($tag->cache_key);
            $count += $res;

            /**
             * удаление из базы идет по событию onKeyForgotten
             * обработка в CacheEventSubscriber::onKeyForgotten
             */
        }

        \DB::commit();

        return $count;
    }

    /**
     *
     */
    static function truncateBase()
    {
        #\DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        Schema::disableForeignKeyConstraints();

        // очистка тегов
        \DB::table(CacheKeyTag::getModel()->getTable())
            ->truncate();

        // очистка ключей
        \DB::table(CacheKey::getModel()->getTable())
            ->truncate();

        #\DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        Schema::enableForeignKeyConstraints();
    }

    /**
     * @param string $key
     * @throws \Exception
     */
    public static function clearBaseByKey(string $key)
    {
        CacheKey::where('key', '=', $key)
            ->delete();
    }

    /**
     * @param string $tag
     * @return bool|null
     * @throws \Exception
     */
    public static function clearBaseByTag(string $tag)
    {
        $res = CacheKeyTag::join('tagged_cache_tags', 'tagged_cache.id', '=', 'tagged_cache_tags.cache_id')
            ->where('tagged_cache_tags.tag', '=', $tag)
            ->delete();

        return $res;
    }

}